# Pile [Jamstack](https://jamstatic.fr/2019/02/07/c-est-quoi-la-jamstack/) pour la gestion du contenu éditorial du site unko.ch

basé sur [Hugo](https://gohugo.io/), [Decap](https://decapcms.org) et [GitLab](https://gitlab.com).

## utilisation

- front-end : https://unko.ch/
- gestion du contenu : https://unko.ch/admin/

## gestion des accès

L'accès à la gestion du contenu se fait au moyen d'un compte [GitLab](https://gitlab.com/users/sign_in).  
L'utilisateur doit avoir au minimum un rôle _Maintainer_ sur le projet [unko](https://gitlab.com/crachecode/unko) ou le groupe [crachecode](https://gitlab.com/crachecode).

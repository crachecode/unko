# TODO

- logo plus grand et en arrière plan
- typo plus lisible pour les titres
- news comme [kraD](https://krad.xyz/news/)

## structure
```
├── config.yml
├── content
│   └── admin
│       └── config.yml
├── layouts
│   └── index.yaml.yml
└── static
    └── admin
        └── index.html
```
---
title: ABSOLUTE VISCERAL DARK_BEAST ARACHNUR ROPE & NOISE « NEW » PERFORMANCE
  ACT & TAPE VERNISSAGE
draft: false
date: 2023-02-16T21:00:26.695Z
place: cave12
description: Arachnur will perform live at cave12, Geneva.
image: /img/2023.02.16_c12-a3_arachnur-pdf.jpg
admin_show: true
publishdate: 2021-02-16T20:00:26.695Z
---
portes: 21h00 / performance: 21h30!

### ARACHNUR (CH)
Leïla Maillard: performance, corde lisse  
nur: son, electronics, voix, performance  
Kiod Bariteau: lumières  
Irene Schlatter: costumes  
Katrine Zingg: perruque

\+

### BAMBA TRISTE
Passage de disque: arach_noise set spécialement pensé pour l’occasion



cave12 :  
Rue de la Prairie 4  
1202 Genf

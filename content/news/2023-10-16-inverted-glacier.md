---
title: Inverted Glacier
draft: false
date: 2023-09-16T00:31:27.845Z
place: Kunstgarage Versam, CH
image: /img/inverted-glacier.jpg
admin_show: true
---
Ein frei improvisiertes Konzert mit experimenteller Musik und Noise. nur spielt mit dem Output von _no-input_, sba0h0r0 experimentiert mit dem Rande der Stimmbänder und sound.codes verleiht Elektrizität eine sinnliche Bedeutung indem er durch mikrototale Landschaften schweift.  
Von und mit: 
nur — elektronics 
sba0h0r0 — Stimme, Windinstrumente
sound.codes — modularer Synthesizer
---
title: Dents De Scie k7 label soirée de soutien
draft: false
date: 2023-05-14T12:07:24.702Z
place: le Pneu, Geneve
image: /img/397850_2023-05-14.jpg
admin_show: true
---
PURPURA 
Une avalanche Harshnoise/Bruja Maya Post Industrial pour terrasser le système. 
chaoticbehaviors.bandcamp.com
 
 
jo ~ oj  
Exploration electronic éclectique dérivant de l'expérimental jusqu'à la musique hindoustanie.  
jo-oj.bandcamp.com
 
 
presque fantôme  
Cassettes chargées de brume, accumulant les boucles ténébreuses en spirales envoûtantes. 
presquefantome.bandcamp.com
 
 
nur 
No reason/no logic/no input, un manifeste sonore nihiliste. 
nurmusic6.bandcamp.com
soundcloud.com
 
 
Monome 
Errance hypnotique dans les territoires cosmiques du drone et de l'ambient. 
monomedrone.bandcamp.com
 
 
Eyyes 
Fascinant flux de synthé cheminant librement dans l'entre-deux du son. 
soundcloud.com
---
title: Gagi Monschter au Grand V
draft: false
date: 2023-04-15T16:50:41.925Z
place: Virieu le Grand
description: Festival de musique, performance et paillette! du 13 au 16 avril
  2023 à virieu le grand
image: /img/grand-v.jpg
link: https://nadanrecordings.bandcamp.com/album/gagi-monschter
admin_show: true
---

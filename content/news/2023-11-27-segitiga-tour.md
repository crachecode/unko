---
title: Segitiga Tour
draft: false
date: 2023-10-23T15:58:36.675Z
place: F/ IT/ CH
image: /img/segitiga-tour.jpg
admin_show: true
---
		
				Segitiga tour Indra Menus / Tzii / nur/  
  


30.10.2023				Kunstraum Walchenturm (Zurich – CH)   
28.10.2023				La Plage (Rompon – FR)   
27.10.2023				Les 9 Salopards (Marseille – FR)   
26.10.2023				Palazzo Bronzo (Genova – IT)   
25.10.2023				Radio Blackout (Torino – IT)   
24.10.2023				La Cata (Grenoble – FR)  
 

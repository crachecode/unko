---
title: Pagel research trip
draft: false
date: 2022-11-11T18:15:21.171Z
place: INDIA
image: /img/i-pagal-trip.jpg
admin_show: true
---
SAYEED & NUR ON THE ROAD 

Mysore - Gokarna - Sangolda - Kohlapur - Morgaon - Shirdi - Dhule - Maheshwar - Bhopal - Sagar - New Yadav Dhaba - Mangawan - Mirzapur - Varanasi - Kolkata

GIGS & JAMS:

03.01.2023                              Walkin Studios (Bangalore - ID)
31.12.2022				New Year’s Party (Mysore – ID)  
xx.11.2022                              Studio Jam (Bhopal -ID)  
20.11.2022				Saraya (Sangolda, Goa - ID)  		
15.11.2022				Beach Jam (Gokarana – ID)  

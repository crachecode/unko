---
title: Solstice Tour
draft: false
date: 2023-12-22T15:19:48.767Z
place: INDIA
image: /img/photo_2024-01-03_19-42-17.jpg
link: ""
admin_show: true
---
**22.12.2023  - Panjim, Goa -NOISEOISEOISE FESTIVAL**\
https://noiseoiseoise.art/  

Paramo Organic Kitchen & Tavern\
परमो ओर्गानिक किचन & टेवर्न\
FVW4+7RC, Ponda - Panaji Rd, Goa 403006, Inde      

**28.12.2023 - Reproduce Vagator - Listening Room**\
https://www.instagram.com/reproduceartists/reel/C1UOgBtvOO5/  

Chakra Tantra Custom Motorcycles, 534, 1, Coutinho Vaddo, Bardez, Vagator, Goa 403509

**FLOW**\
**615-645 Parshuraavan x Nur**\
**645-715 Jay Kshirsagar**\
**715-800 Excise Dept**\
**815-900 Shehzor**  

**06.01.2024 - Bangalore - Shoonya - Centre for Art and Somatic Practices**  

 4th Floor, Rear Wing, Brahmananda Court #37, Lalbagh Road Bangalore 560027 India
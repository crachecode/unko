---
title: Jogja Noise Bombing & mini tour
draft: false
date: 2017-01-15T17:09:48.685Z
place: SAUNG GARLIC RESTO Jogja
description: Jogja Noise Bombing
image: /img/jogja-noisebombing-2017.png
admin_show: true
---
05/02/17 @ somewhere outdoor (Cirebon – IDN)  
19/01/17 @ Houten Hand (Malang – IDN)  
16/01/17 @ Loby J kampus Isi (Surakarta – IDN)  
15/01/17 @ Saung Garlic / Jogja Noise Bombing Fest (Yogyakarta – IDN)  
10/01/17 @ Ruang Gulma (Bantul – IDN)  

---
title: Jogja Noise Bombing
draft: false
date: 2018-01-21T16:55:09.381Z
place: Yogyakarta, Barcode Kitchen
image: /img/noisebombing-2018.jpg
link: https://archive.org/details/JogjaNoiseBombingFestival2018LiveRecording
admin_show: true
---
Jogja Noise Bombing Festival 2018  
  

20–21 Januari 2018  
Barcode Kitchen and Bar  
Jl. R.W. Monginsidi No.23, Karangwaru, Tegalrejo  
Yogyakarta  



https://seismograf.org/en/artikel/noise-tropical-underground  
  
  

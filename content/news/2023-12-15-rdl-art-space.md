---
title: BEBUNYIANX II . GIG NOISE EKPERIMENTAL
draft: false
date: 2018-02-02T17:22:54.280Z
place: Ruang Dulapan Art Space, Palu
description: ""
image: /img/indo-flyer.jpg
admin_show: true
---
BEBUNYIANX II, Adalah sebuah gelaran gig noise eksperimental, yang digarap secara kolektif oleh beberapa pekerja dan atau komunitas kreatif yang ada di kota Palu.
Gelaran sederhana ini akan menampilkan pertunjukan musik ekperimen dari beberapa musisi Indonesia dan Eropa yang di kemas melalui proyeksi visual. 
Save  Da Noise !
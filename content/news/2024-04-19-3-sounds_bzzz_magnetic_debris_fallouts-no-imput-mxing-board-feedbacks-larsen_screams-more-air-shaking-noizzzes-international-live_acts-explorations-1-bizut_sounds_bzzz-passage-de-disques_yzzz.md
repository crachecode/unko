---
title: SOUNDS_BZZZ_MAGNETIC_DEBRIS_FALLOUTS
draft: false
date: 2024-06-12T14:06:00.000Z
place: "cave12, Genf (CH) "
description: 3 SOUNDS_BZZZ_MAGNETIC_DEBRIS_FALLOUTS, NO IMPUT MXING BOARD
  FEEDBACKS, LARSEN_SCREAMS & MORE AIR SHAKING NOIZZZES INTERNATIONAL LIVE_ACTS
  EXPLORATIONS + 1 BIZUT_SOUNDS_BZZZ PASSAGE DE DISQUES_YZZZ!!! -
image: /img/photo_2024-06-14_13-24-00.jpg
link: https://www.cave12.org/
admin_show: true
---
MERCREDI 12 JUIN - portes: 21h00/concerts: 21h30!!!

TOSHIMARU NAKAMURA & KJELL BJORGEENGEN (Japon/Norvège)  

Toshimaru Nakamura (Japon): no input mixing board  

Kjell Bjørgeengen (Norvège): signal vidéo traité  

* ANTIMATTER (Usa)  

Dispositif électronique  

* nur (CH)  

No input mixing board, feedback, voix  

* PASSAGE DE DISQUES  

Bamba Triste: bizut_sounds_bzzz set spécialement pensé pour l’occasion  

Palpitant et grésillant mercredi soir de bizutage_sonic_noise_bzzz comme on les aime ici avec des musicien.ne.s/performeur.se.s hautement aguerri.e.s/activistes du genre, provenant de trois continents différents et décliné en trois actes_lives + un passage de disques, comme suit (ordre de passage non défini):

TOSHIMARU NAKAMURA & KJELL BJORGEENGEN (Japon/Norvège)

TOSHIMARU NAKAMURA et KJELL BJORGEENGEN ont tous les deux été des pionniers hautement distinctifs dans leurs méthodes et moyens/instruments d’appréhender le son et la vidéo.

Précurseur incontesté et incontestable du no imput mixing board (une table de mixage bouclée sur elle-même), le tokyoïte TOSHIMARU NAKAMURA est une figure historique incontournable et à l’impact considérable issu  de la scène exploratoire aventureuse japonaise. Utilisant sa table de mixage comme un instrument électronique en soi en produisant du son sans aucun signal-input audio externe, TOSHIMARU NAKAMURA a littéralement inventé un nouveau langage et une méthode de générer des sons avec une approche totalement inédite/inouÏe, internationalement acclamée par ses pairs et la critique spécialisée, faisant de lui une référence incontournable dans son domaine.

KJELL BJORGEENGEN de son côté explore les signaux vidéos à travers divers appareils/dispositifs, capturant les débris-tombées magnétiques de la video en les convertissant en son.

En live, les sons de TOSHIMARU NAKAMURA nourrissent le synthétiseur vidéo de KJELL BJORGEENGEN influençant ainsi oscillateurs et traitements vidéos. La sortie vidéo en résultant est à son tour retransformée en son, générant un feedback dans la table de mixage de NAKAMURA qui a son tour reretransforme la boucle qui se crée, générant une sorte de feedback infini et continuellement alimenté.

Cet échange hyper-dynamique génère ainsi un circuit où chaque mouvement/décision de l’un des protagonistes impacte automatiquement l’autre, créant une situation de précarité-instabilité-fragilité extrême résultant en un bain sonore de grésillements parasitaires imprévisibles captivant, constamment sur le fil du rasoir, demandant une acceptation totale de l’indétermination du sytème et des surprises qu’il provoque à chaque instant. Un système que TOSHIMARU NAKAMURA et  KJELL BJORGEENGEN apprivoisent avec une maîtrise à chaque fois renouvelée.

Un travail fascinant, plongeant et jonglant avec malice et haute-complicité au coeur d’un langage de débris parasitaires constamment stimulé et en mouvement imprévisible/indéterminé

ANTIMATTER (Usa)

Projet solo de l’étasunien Xopher Davidson, (ancien long complice de route d’un certain… Zbigniew Karkowski), ANTIMATTER plonge l’auditeur.trice.x dans une exploration au plus profond de l’espace-son, de ses distortions de la gravité et des échelles, de ses longueurs d’ondes s’étendant sur tout l’univers tout en étant affectées par des micro-perturbations temporelles, de ses systèmes instables opérant en dehors de leurs spécifications & buts attendus, de ses motifs-interférences magnifiés provoquant éventuellement un switch entre les positions du négatif et du positif, l’arrière-plan devenant le premier plan. Une vague dans une vague dans une vague… chacune avec son propre univers et dont la source nous est inconnue, sans emplacement pré-fixé, un tournoiement constant de la matière et de l’antimatière.

Un dense bain sonore secouant l’air tout en nous englobant avec intensité. 

nur

Du no inpute funèbre célébrant par avance la mort de la politique gerbant des frontières historikes et aktuelles

 Des coups de feedbacks furieuX contestant la doctrine de l'apparance à l'origne de toute sorte de classements totalitaires depuis les debuts des temps

Get over the body lizard feedback à l’état pur & larsens stridents éructants. Listen! Or don't.

but 

STOP GENOCID IN GAZA NOW

Le tout enrubanné comme il se doit par les passage de disques bizut_sounds_bzzz de Bamba Triste_yzzz!

Plus d’infos:

TOSHIMARU NAKAMURA:

https://www.toshimarunakamura.com/home

KJELL BJORGEENGEN:

https://bjorgeengen.com/

ANTIMATTER:

https://antimatter3.bandcamp.com/

NUR:

https://nur-soundz.bandcamp.com

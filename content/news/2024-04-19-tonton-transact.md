---
title: TONTON  transact
draft: false
date: 2024-05-24T14:03:00.000Z
place: Buffet Nord, Bern (CH)
image: /img/transact_04.jpeg
link: https://www.buffetnord.ch/event/tonton-transact-3
admin_show: true
---
TONTON transact



   Richtpreis 25.00 



Nach einer wunderbaren TONTON Saison und bevor die Sommer-Pause beginnt, feiern wir das Ende vom Anfang – «TONTON-transact» nennt sich das, ein Mini-Festival mit einem Konzertabend im Buffet Nord – und wenn möglich draussen.



TONTON bringt Künstler*innen aus den Bereichen der elektronischen und analogen Musik, der Performance, der visuellen Darbietungen wie auch der bildenden Kunst zusammen und lässt so neue Klangwelten oder auch Neues fürs Auge und Ohr entstehen.



TONTON transact w/ Arthur Hnatek & Björn Meyer || nur & L V K K || Buatoma & zozoTransistor



Ab 18h Bar und leckere Krapfen

---
title: "#LSD75 Eleusis"
draft: false
date: 2018-04-19T09:02:00.000Z
place: Basel
description: "75th anniversary of Albert Hofmann´s incredible consciousness
  transforming discovery "
image: /img/20180419_75-jahre-lsd-jubilaeum-eleusis_20180311202652.jpg
link: https://www.goabase.net/de/party/75-jahre-lsd-jubilaeum-lsd75-eleusis/99277
admin_show: true
---

music // live
Ajja, Zen Baboon, WIDE AWAKE, Estas Tonne Music, Mich Gerber, The Konincks, The Legendary Lightness, namaka
Sons of Morpheus plays Jimi Hendrix,@ChillyTrippyDippy,
All Ship Shape, Len Sander, The Return Of Bigton, L Twills, Joel Olivé, Samuel Dread J Elmiger, uvm.
music // dj
Niju, LOKKE, Timboletti, Donna Luetjen , Lina Charlotte, Julian Ohne Fuchs, E И E L, Koray Yildirim uvm.
performance // art
Little swastika - psyland, Mark Paul Divo,@Tanja Roscic, Youlia Lia, Ivan Merlin, Irma Hirsl, Maya Minder, Jaguar on the Moon, Twen D. Vari, Murmel Design, Fanny Yemmely uvm.
workshop //
Svea Soleil, Vanja Palmers, Lucius Wertmüller, Joel Olivé, Pho Tona
Info
#LSD75 a social sculpture !

www.lsd75.ch

75th anniversary of Albert Hofmann´s incredible consciousness transforming discovery - a 4 day symposium event in Switzerland, with renowned speakers, stimulating workshops, round table discussions, science, art, music, the chance to repeat the original bike ride on the exact route, plus more.

Vom 19. bis 22. April 2018 organisiert die Psychedelic Society Switzerland im Holzpark Klybeck die 75-jährige Entdeckung des LSD. Dabei wird u.a. von Künstlern, Wissenschaftlern und Besuchern eine soziale Skulptur aus Vorlesungen, Kunstinstallationen, Happenings, Ausstellungen, Konzerten, Workshops, Performances und Marktständen initiiert.
Im Zentrum stehen der Austausch und Dialog sowie eine transdiziplinäre und moderne psychonautische Auseinandersetzung der Initiationsriten der griechischen Mysterien von Eleusis. Alle sind herzlich eingeladen, ein Teil davon zu werden.

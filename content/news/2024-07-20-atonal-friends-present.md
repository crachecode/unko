---
title: aToNaL & FRIENDS present
draft: false
date: 2017-06-22T10:11:00.000Z
place: Umbo
description: Simon Whetham, B°Tong, Lorenzo Abattoir, NuR
image: /img/22_juni_umbo-beide_kl.jpg
link: https://radar.squat.net/de/event/zurich/umbo/2017-06-22/simon-whetham-bdegtong-lorenzo-abattoir-nur-atonal-friends-present
admin_show: true
---
Simon Whetham (Bristol)

From working with field recordings to exploration of the acoustic qualities of a space and resonances of objects, Simon Whetham composes and performs immersive collages, both meditative and thought provoking...  
www.simonwhetham.co.uk
  


B°Tong (Basel)

Record Release Event – B°tong just returned from his third US tour and will work this year on music for the theatre-piece “vertikal”. This spring two new LP’s are released on a new spanish label.
B°tong is Chris Sigdell, an experimental electronic musician. He developed a soft spot for early industrial textures and pioneering ambient soundscapes while cutting his teeth in cult industrial band NID. With B°tong he reaches for the nether regions of experimental ambient sound and established himself as an acknowledged professional soundscape artist. His music is a choice of dark, brooding layers of sound, high-pitched tones and weird electronic sounds that give birth to images of darkness and tranquillity, the solitude of an icy polar night or the equivalent of an underwater journey in a bottomless pit. To create these "imaginary soundtracks to the inner eye", he uses samples and field-recordings, which he processes in his computer-programs together with recordings of his own voice and/or other vocal samples. There are no hand-played instruments involved. In a live-situation the computer is taboo - Chris Sigdell uses microphone, metal, electric gadgets and various kitchen utensils. The sounds thus generated are put through various effect-pedals giving him that trademark B°tong sound, which has earned him the nick-name “dronelord” by Aquarius Records.  

btongmusic.wixsite.com/btong

btong.bandcamp.com

facebook.com/btongmusic
  
  
  


Lorenzo Abbatoir (Torino)

Lorenzo Abattoir is an Italian-based noise artist and sound engineer from Torino. His work takes shape in a context of underground extreme music to become accessible through various projects related to contemporary art. During past years he recorded and released some full-length albums and various collaborative albums with other artists: PSICOPOMPO with German composer Hermann Kopp (4iB records), MARE DI DIRAC electro-acoustic collective (Dusktone Records), a collaboration with PHURPA “Misericordia” Vinyl LP (4iB records) and a new collab. with SATORI "Aether", digipack CD will be out on March 2017 (Old captain and Malignant Records co-production).  

http://lorenzoabattoir.blogspot.it  

https://www.youtube.com/watch?v=kjg6IlXFJpk


NuR (Genève)

Traffucked field recordings. Severely screwed and always delayed recordings due to high levels of compression. a dj set for the last ones and the retarded.  


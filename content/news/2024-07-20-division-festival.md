---
title: Division Festival
draft: false
date: 2018-07-31T09:08:00.000Z
place: Montrelais France
image: /img/divisionlogonb.jpg
link: https://www.thirdtypetapes.com/events/view/43/31-07-2018/DIVISION
admin_show: true
---
TTT se met au vert Dans la Dépendance et invite des ami-e-s pour deux jours de fête en bord de Loire les 31 juillet et 1 août.

En vrac : des concerts, des mixes, des projections sur la Loire, des installations sonores, une dégustation de vins naturels (yen aura au bar aussi bien sûr), une cantine végan, un atelier de gravure de plexidisques, une éclade de moules géante, des distros. 

10 lignes de Blings, 10 lignes de Blangs

6RME

Accou

A Ten Year Winter

à travers

BaBa YaGe

Bear Bones Lay Low

Blenno Die WurstBrucke

Bobb de Lost Dogs

Bobé Van Jézu

c_c

Cabine Volcan

Carrageenan

Le Compas Dans L'Oeil

Descendeur

Dj Sainte Rita

Goz Pan Zoo

Guilhem All

Henry Dèche

IlEstChelouTonSon

Jean Bender

Juste Oreilles

Letal Ataraxia

Me Donner

Mlacoler Culkin

Nuances D'Engrais

NuR

Odium Decoy (aka Unas BM)

Paolo Técon

Pierre Gordeeff

Ponge

Ripit

Rosa Canina

Tabasse Rumba

Thomas Tilly

Tzii

Somaticae

Sudrë

Zero Pixel

Zohastre

 

Une plage, des arbres, deux sonos.

Participation Au Frais : 10 balles par jour, 20 balles les deux jours

Bar prix sympas

Cantine vegan prix libre

Possibilité de camper sur place

Accès en voiture

---
title: SOON
draft: false
date: 2018-12-15T09:49:00.000Z
place: Le Spoutnik
description: performances audiovisuelles noise
image: /img/soon-affiche-large.jpg
admin_show: true
---

Soirée de performances bruitistes et vocales avec un workshop, quatre sets, et un final en projection avec un film cyberpunk japonais culte, le tout placé sous le sigle du vacarme (sōon).  

Line Up:   

Kazehito Seki
...


projection de Pinocchio √964 
tarif unique 5CHF
Inclut dans l’entrée, rabais de 5CHF pour la soirée du ZOO: Psychedelik Language
  

Kazehito Seki est un artiste vocal et musicien japonais qui propose une utilisation unique de ses cordes vocales avec un spectre allant de l’ambiance la plus subtile au harsh noise le plus brutal. Nous profitons de sa venue au passage d’une tournée qu’il fait avec l’artiste glitch noise suisse . . . (aka Nikola Mounoud) très prisé dans le milieu noise japonais qui propose une performance avec les visuels d’oscilloscopes virtuels de Maszkowicz. Ces deux sets seront précédé d’une performance audiovisuelles de Tzii et NuR el tropiko Gothiko, avec des entre actes proposés par Nelson Landwehr. 


Réalisation
	
Shozin Fukui
Pays
	
Japon
Année
	
1993
Langue
	
ST anglais
VO anglais
Format
	
Fichier Numérique
Avec
	
Hage Suzuki
Onn Chan
Cycle
	
Performances
Soirées 

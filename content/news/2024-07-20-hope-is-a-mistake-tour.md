---
title: Hope Is A Mistake Tour
draft: false
date: 2017-07-25T10:17:00.000Z
place: From La Chaux de Fonds to Istanbul
description: Tzii&NuR on Tour
image: /img/photo_2024-07-21_09-49-14.jpg
admin_show: true
---

25.07.2017		Жижа / Žiža (Banja Luka - BA)
26.07.2017		Fabrika (Novi Sad - RS)
28.07.2017		Termokiss (Prishtina - XK)
30.07.2017		Fabrika Avtonomia (Sofia - BG)
03.08.2017		Arkaoda (Istanbul - TUR)
09.08.2017		Yeşilçam Sineması (Istanbul - TUR)
11.08.2017 	        Studio 11 (Subotica - RS)
12.08.2017		SUB (Vienna - AT)
18.08.2017		ADM festival (Amsterdam - NL)

   

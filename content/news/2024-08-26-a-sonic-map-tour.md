---
title: A Sonic Map Tour
draft: false
date: 2018-11-16T18:00:00.000Z
place: CHINA
description: 2M2 / PURPURA / TOKAR / NUR ON TOUR
image: /img/sonic-map-good.jpg
link: https://www.facebook.com/events/326737578108869/
admin_show: true
---
18.11	Soup (Tokyo - JPN)  
16.11	SAAL (Hong Kong)  
15.11   Blackiron (Nanchang - CN)  
14.11	Ontheway (Hefei - CN)  
13.11	Vox (Wuhan - CN)      
12.11	Blindlobby (Wenzhou - CN)  
11.11   GEBI (Yiwu - CN)  
10.11	Mao Electronics (Shanghai - CN)  
09.11	??? dj nureonna (Beijing - CN)         	
08.11   YUE Space (Beijing - CN)  


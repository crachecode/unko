---
title: "Hackedepicciotto (DE/US), support: nur"
draft: false
date: 2022-03-12T19:00:00.000Z
place: Dampfzentrale, Bern
image: /img/header_hackedepicciotto_credit_sven_marquardt_01.jpg
admin_show: true
---
Hackedepicciotto (DE/US)
Support: NuR. Danach: nuRE onNA (DJ-Set)
Alexander Hacke (Einstürzende Neubauten, Crime & The City Solution) & Danielle de Picciotto (Crime & The City Solution)


20:45 NuR
21:15  Hackedepicciotto


Hackedepicciotto

«Wir sind fast in allem gegensätzlich», sagt Danielle de Picciotto über die Beziehung zu ihrem Lebenspartner Alexander Hacke. Er ist seit dem Gründungsjahr der Band Mitglied der Einstürzenden Neubauten, sie hat 1989 mit ihrem damaligen Freund die Berliner Love Parade gegründet. Mit «The Silver Threshold» haben Hacke und de Picciotto nun ihr viertes gemeinsames Album herausgegeben; das erste für das Label Mute Records, welches sich auch um die Veröffentlichungen von Depeche Mode oder Nick Cave & The Bad Seeds kümmert.  

«The Silver Threshold» gleitet von cineastischen Themen über tribal-follkoristische Passagen und sphärische Ausläufer hin zu eruptiven Krachern – dies aber immer im Gewand des Songformats und meist getragen vom Sprechgesang von Danielle de Picciotto. Das Albumcover und die Promofotos stammen von Sven Marquardt, dem Berliner Fotografen und berüchtigten Türsteher des Berghain.  
  

NuR

Thee total sonic fallout, conducted by traffucked, severely screwed and always delayed sphere recordings, saturated with no reason, no logic and no input.

---
title: SLEEP CONCERT
draft: false
date: 2022-06-12T22:00:00.000Z
place: Le Commun, GE
description: organisé par Bamba Triste
image: /img/283243513_4894513890660732_7263927982835391998_n.jpg
link: https://www.facebook.com/bambatriste666/
admin_show: true
---
Artiste locale habituée des expérimentations artistiques hybrides, NuR a exploré à travers ses multiples collaborations la musique, la danse et plus récemment la performance capillaire… Mais bon, pour cette occasion unique, elle s’en tiendra au son: enregistrements spatialisés trafiqués, nappes de synthétiseurs fantomatiques, des retards sur retards sur retards sur retards.  
Sans raison, sans partition, voguant sur les flots de la nuit, elle piratera nos esprits et nous accompagnera explorer nos rêves.
  
https://fb.me/e/d11A6QFhj  
   
Organisé dans le cadre de l'exposition Renaissance, sur le lien entre art et algorithmique: http://renaissance.ooo/     

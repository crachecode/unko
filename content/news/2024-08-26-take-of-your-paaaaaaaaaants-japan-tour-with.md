---
title: Take of your paaaaaaaaaants  Japan Tour with ...
draft: false
date: 2019-06-30T17:00:00.000Z
place: Vietnam - China -Taiwan - Japan
image: /img/2019-japan-2019.jpg
admin_show: true
---
11.08	Tokyo		Otooto  
10.08	Tokyo		SOUP    
08.08	Sapporo		Sound Crue  
07.08   Koriyama	PEAK ACTION  
06.08	Chiba		Jazz spot Candy    
04.08   Nagoya		Spazio Rita  
03.08	Osaka		Environment Og  
01.08	Osaka		Namba Bears  
31.07	Kyoto		Socrates    
30.07	Tokushima	Txalaparta  
29.07   Takamatsu       TOONICE  
28.07	Hiroshima	Public Bar Roots  
27.07	Kochi		Onzo    
26.07	Matsuyama	Oto-Doke	  
25.07	Fukuoka		Gigi Bar      	  	
24.07	Nagasaki	Sumire-sha  
23.07	Fukuoka		UTERO      
22.07	Kitakyushuu	Megahertz    
21.07	Oita		AT HALL    
20.07	Omuta		Omuta Fuji   
19.07	Kagoshima	El Sonido    
17.07	Okinawa		Sound M'S      
14.07	Taipei		Revolver  
12.07	Taichung	littleplay  
11.07	Taipei		Outer Pulsation (under a bridge)      	
10.07	Hsinchu		Jiang Shan Yi Gai Suo  
08.07	Shanghai	Lofas  
07.07	Hangzhou	LOOPY     
06.07   Hong Kong	SAAL  
05.07   Macao		LMA  
30.06	Hanoi		A Space – An Experimental Art    

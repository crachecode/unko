---
title: Consumo Democratico 3
draft: false
date: 2021-11-10T19:00:00.000Z
place: UMBO, Zürich
image: /img/2021_consumo-democratico.gif
admin_show: true
---
Konzerte:

Jazzoux (Amiens/St. Étienne)

Dave Phillips "Field Recordings" (Zürich)

NuR (Genève)

‍

Ein Turm. Dancing dogs und Techno non-genre fait maison mit den neuen Duo Jazzoux von Claire Gapenne (Terrine, Headwar) und Amédée (Somaticae).

Tiefes Eintauchen in die bewegten und mutierten Klangwelten von Dave Phillips' "field recordings".

Funeral Electronics Promenade mit NuR. 

---
title: Ineptias Momentum Tour with Kazehito Seki, ..., Tzii et NuR
draft: false
date: 2018-12-13T17:00:00.000Z
place: FRANCE - SUISSE - ALLEMAGNE - PAYS BAS - BELGIQUE
image: /img/2018-ineptias.jpg
admin_show: true
---
22.12.2018		Walter (Brussels - BE)   
21.12.2018		OCCII (Amsterdam - NL)  
20.12.2018		Djäzz (Duisbourg - DE)  
19.12.2018		Institut Fur Neue Medien (Frankfurt - DE)  
18.12.2018		Bunkr (Zurich - CH)    
16.12.2018		Le Lac (La Chaux-De-Fonds - CH)  
15.12.2018		Spoutnik (Genève - CH)  
14.12.2018		Aencre (Renens -CH)  
13.12.2018		L'Atelier de l'Étoile (Besançon - FR)  

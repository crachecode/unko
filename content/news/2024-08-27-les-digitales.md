---
title: Les Digitales
draft: false
date: 2018-08-18T16:00:00.000Z
place: Christoph-Merian-Park, Basel
image: /img/ld2018_flyer_basel.jpg
admin_show: true
---
LINE UP:  
Holy Oscillators FR CH
Stéphane Montavon BS CH
Gilles Aubry JU CH  
Laurent Güdel & Francisco Meirino VD CH  
Feine Trinkers bei Pinkels Daheim Bremen DE  
Nur GE CH  
Steiner BS CH  
The Mistalch JU CH  
  


Dieses Jahr findet das erste Les Digitales in Basel im Christoph-Merian-Park statt. Die bahnhofsnahe und grüne Idylle gleich neben dem Sommercasino, bietet einen idealen Rahmen, um über ein quadrofonisches Mehrkanal-System den live erzeugten Klängen zu lauschen. Das Programm mit lokalen und internationalen Acts präsentiert ein vielschichtiges und exquisites Klangerlebnis der avant­gar­dis­tischen, experimentellen und elektronischen Musik.  

Für Speis und Trank sorgt das Gastro-Team der renommierten Kulturbeiz 113 aus dem Warteck in Basel. In Zusammenarbeit den Veranstaltern von Klappfon, verspricht die erste Runde in der Rheinstadt ein musisches Arrangement für offene und neugierige Ohren, Freunde, Bekannte und die nimmersatten JSG-Gänger (es hat Liegestühle).  

---
title: "Nout presents: From experimental to techno"
draft: false
date: 2022-05-07T17:00:00.000Z
place: L'écurie, Genève
image: /img/2022_05_07.jpg
link: https://soundcloud.com/nout_agency
admin_show: true
---
LINE UP:  
Nathalie Rebholz (live)  
Nur (live)  
OWELLE (live)  
JOLLY (DJ set)
STRB (DH set)

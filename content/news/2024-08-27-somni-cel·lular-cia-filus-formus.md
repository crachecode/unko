---
title: Somni cel·lular - Cia. Filus formus
draft: false
date: 2018-09-24T17:00:00.000Z
place: Museu de Ciències Naturals  (Barcelone - ESP)
image: /img/2018-09-24_somni_cel-lular.jpg
link: https://www.facebook.com/filusformus/
admin_show: true
---
Somni cel·lular

Performance que proposa un joc d’interacció entre el cos humà, l’escultura i l’entorn arquitectònic.  

A la ciutat, la identitat de l’ésser humà s’exacerba. L’humà usa la seva paleta de condicionaments per sobreviure i ser reconegut pels seus companys: la roba, la manera de caminar, de parlar, l’accent, el maquillatge, el pentinat, els amics, els colors… Si tornem a l’origen, a l’humà com a animal, com a conjunt de molècules, de formes, de fluxos energètics, i ho posem en el mitjà d’ésser desconeguts i orgànics, què passa?  

A càrrec de: Cia Filus Formus
Dansa: Lili Lenfant  
Música: Nur  
Direcció artística i escultura: Jessica Moroni  

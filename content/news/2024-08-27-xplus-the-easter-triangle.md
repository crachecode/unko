---
title: XPlus & The easter triangle
draft: true
date: 2022-04-14T18:00:00.000Z
image: /img/affiche_legroove_14_04_22.jpg
link: https://konnekt.co/fr/agenda
admin_show: true
---
Live band à géométrie variable, le projet Xplus sera au centre d'une soirée de concerts utilisant un système de diffusion en triangle △, source de nombreux effets de spatialisation hypnotique.  
Konnekt propose une soirée festive aux dispositions d’écoutes exceptionnelles, durant laquelle le public sera libre de déambuler d’un bout à l’autre de ce triangle de haut-parleurs, s’entremêlant avec les musiciens.
Trois artistes électroniques en solos partageront la soirée avec le projet Xplus et ses riffs tourbillonnants.  

Line-up :  

21H30 - Sonia P
22H15 - NuR  
23H00 - Xplus
00H30 - Vincent Haenni  
01H30 - HTN (Brainwaves)  


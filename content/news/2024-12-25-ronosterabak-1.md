---
title: "RonosTerabak#1 "
draft: false
date: 2020-10-11T15:00:00.000Z
place: Le Pneu
description: écoute électroacoustique
image: /img/ronos_terabak_1.jpg
admin_show: true
---
Séance d’écoute dimanche 11 octobre 2020  

15 artistes – 9 sets

entrée prix libre

ouverture des portes 15h

fin 20h

after à la cave 12 dès 21h

Le Pneu – Vldr

rue du Vélodrome 18 – 1205 Genève

**15h30 HARMONIUM SOLO**

D.C.P

https://deafdcp.org/

Acoustic drones

**16h OLO – SOMBRE**

Loïc Grobéty

www.loicgrobety.ch

During this obscure or even uncertain period, the pieces of – Sombre –
 offer his sound vision of the moment. He creates epic, powerful and 
avant-garde landscapes within the confines of doom, drone and noise

**16h30 KUDETA**

MDLR synthesizer : Ricardo da Silva

Guitar & Electronics : Sébastien Chaignat

Kudeta est un duo de musiciens  évoluant dans la fange du cosmos 
musical. D’abord expressifs visuellement, Ryk et Seb développent des 
cinématiques sonores depuis 2010 comme autant de travellings sonores 
entrecoupés d’éclairs. Kaosmose anthropo-machinique du soulèvement 
planétaire et ode à la vie.

**17h SUSPENDED**

Performance : NuR et Beau

Body Sonification : Daniel Maszkowicz

Hang, hold and drones

**17h30 MASPIN feat. PURPURA**

Violon & Power Electronics

jamaspjhabvala.tumblr.com/maspin

chaoticbehaviors.bandcamp.com/

Maspin is a violin solo project, mix of rock, ambient, noise and 
balkan influences, evoking various landscapes. His collaboration with 
Purpura is bringing abstract electronic and heavy beats into the loop, 
in a neverending ritual full of hisses, clamors, seisms and smoke.

**18h KOKHLIAS & LOULAN – OBSCURE SINGING**

applied spatial geomorphology

vibrational phenomena & ear-voice

https://humankindrecords.bandcamp.com/track/venus-venera

https://soundcloud.com/antoinelang/obscure-singing

расплывчатость терминов

vazio rugindo

turvação de termos

выйди на улицу

vá lá fora                     ревущая пустота

feche a porta

закрой дверь

**18h30 NOIJZU**

Shuyue Zhao, Clarinet (CH, CN)

Luis Sanz, Computer Music (CH, PE)

https://bit.ly/2SkVGgv

Noijzu is a Swiss experimental unit formed by Shuyue Zhao and Luis 
Sanz. The duo focuses on acoustic sounds and computer-generated sonic 
entities. Noijzu blends bass clarinet with digital signals, the result 
consists of extreme acoustic dynamics, complex and hyperreal textures, 
ranging from minimal to extreme and confrontational sound expressions.

**19h KAZEHITO SEKI**

Electrifeeding Voice (JP)

https://kaze.a-noise.org

Noise musician, acoustic acupuncturist, or whatsoever. Titled Self 
Toxication is Kazehito’s solo musical project. His in-the-moment, 
site-specific live experiments that utilise the organic materials and 
catalysts of his body, mind, extended voice, amplification, feedback 
through re-amplification, room acoustics, and the existence of the 
audience (if there is one,) creating an observation on what noise music 
can be. Noise music (art) is a possibility for spectra instead of 
binaries. Nothing is good or bad without setting a specific context – 
observing something, there is always another point of view.

**19h30 SEMOROZ/SOUHARCE**

aaaah noise (FR, CH)

https://juliesemoroz.ch

http://www.emmasouharce.com

The two artists have been collaborating since 2018 with the duo 
Effraction Vacances, an improvised live play format based on mainstream 
pop music from the 1990/2000 – different each time – setting it up as a 
decomposed sculpture. For this new form, Semoroz and Souharce are 
focusing on the possibilities of nested different timings in live, 
playing on large dynamic ranges inspired by both Instagram culture and 
drone music.

function hideMenu() {
  document.querySelector('body > .content > header').classList.remove('opened')
}

document.addEventListener('DOMContentLoaded', () => {
  let sandwich = document.querySelector('#sandwich')
  if (sandwich) {
    sandwich.addEventListener('click', () => {
      document.querySelector('body > .content > header').classList.toggle('opened')
    })
  }
})
